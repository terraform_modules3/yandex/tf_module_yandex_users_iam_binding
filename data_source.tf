
data "yandex_client_config" "client" {}

data "yandex_resourcemanager_folder" "user_folder" {
  for_each = { for folder in local.user_folder_names : folder => folder }
  name     = each.key
  cloud_id = local.cloud_id
}

data "yandex_iam_user" "user" {
  for_each = { for user in local.users : user => user }
  login    = each.key
}
