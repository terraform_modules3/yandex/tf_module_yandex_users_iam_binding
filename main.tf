

locals {
  cloud_id = var.cloud_id == null ? data.yandex_client_config.client.cloud_id : var.cloud_id
}

locals {
  user_roles_folder_names = flatten([
    for entry in var.user_bindings :
    [
      for role in entry.folder_roles : role.folder
    ]
  ])

  user_folder_names = distinct(local.user_roles_folder_names)

  users = flatten([
    for entry in var.user_bindings : entry.user
  ])

  user_folder_roles_map = { for item in
    flatten([
      for binding in var.user_bindings :
      [
        for folder_role_list in binding.folder_roles :
        [
          for role in folder_role_list.roles :
          {
            key   = "${binding.user}:${data.yandex_resourcemanager_folder.user_folder[folder_role_list.folder].folder_id}:${role}"
            value = role
          }
        ]
      ]
    ])
    : item.key => item.value
  }

  user_cloud_roles_map = { for item in
    flatten([
      for binding in var.user_bindings :
      [
        for cloud_role in binding.cloud_roles :
        {
          key   = "${binding.user}:${cloud_role}"
          value = cloud_role
        }
      ]
    ])
    : item.key => item.value
  }
}

resource "yandex_resourcemanager_folder_iam_member" "user_binding" {
  for_each = local.user_folder_roles_map

  folder_id = split(":", each.key)[1]
  role      = each.value
  member    = "userAccount:${data.yandex_iam_user.user[split(":", each.key)[0]].id}"
}

resource "yandex_resourcemanager_cloud_iam_member" "user_binding" {
  for_each = local.user_cloud_roles_map

  cloud_id = local.cloud_id
  role     = each.value
  member   = "userAccount:${data.yandex_iam_user.user[split(":", each.key)[0]].id}"
}
