# Yandex Cloud User IAM Binding Terraform Module
Allows to create user binding in Yandex cloud

## Usage example
```hcl
module "user_iam_binding" {
  source = "git::https://gitlab.com/terraform_modules3/yandex/tf_module_yandex_users_iam_binding.git?ref=0.1.0"

  user_bindings = [
    {
      user = "user1@email.com"
      cloud_roles = [
        "resource-manager.clouds.member",
      ],
      folder_roles = [
        {
          folder = "dev",
          roles  = ["viewer"]
        },
        {
          folder = "default",
          roles  = ["viewer"]
        },
      ],
    },
    {
      user = "user2@email.com"
      cloud_roles = [
        "resource-manager.clouds.member",
      ],
      folder_roles = [
        {
          folder = "prod",
          roles  = ["viewer"]
        },
        {
          folder = "dev",
          roles  = ["viewer", "storage.editor"]
        },
      ],
    },
  ]
}
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | > 0.90 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | 0.113.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [yandex_resourcemanager_cloud_iam_member.user_binding](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_cloud_iam_member) | resource |
| [yandex_resourcemanager_folder_iam_member.user_binding](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) | resource |
| [yandex_client_config.client](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/client_config) | data source |
| [yandex_iam_user.user](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/iam_user) | data source |
| [yandex_resourcemanager_folder.user_folder](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/resourcemanager_folder) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloud_id"></a> [cloud\_id](#input\_cloud\_id) | n/a | `string` | `null` | no |
| <a name="input_user_bindings"></a> [user\_bindings](#input\_user\_bindings) | Example:<br><br>    user\_bindings = [<br>      {<br>        user = "user1@email.com"<br>        cloud\_roles = [<br>          "resource-manager.clouds.member",<br>        ],<br>        folder\_roles = [<br>          {<br>            folder = "dev",<br>            roles  = ["viewer"]<br>          },<br>          {<br>            folder = "default",<br>            roles  = ["viewer"]<br>          },<br>        ],<br>      },<br>      {<br>        user = "user2@email.com"<br>        cloud\_roles = [<br>          "resource-manager.clouds.member",<br>        ],<br>        folder\_roles = [<br>          {<br>            folder = "prod",<br>            roles  = ["viewer"]<br>          },<br>          {<br>            folder = "dev",<br>            roles  = ["viewer", "storage.editor"]<br>          },<br>        ],<br>      },<br>    ] | <pre>list(object({<br>    user = string<br>    folder_roles = optional(list(object({<br>      folder = string<br>      roles  = list(string)<br>    })), [])<br>    cloud_roles = optional(list(string), [])<br>  }))</pre> | `[]` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
