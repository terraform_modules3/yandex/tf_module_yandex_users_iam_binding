variable "cloud_id" {
  type    = string
  default = null
}

variable "user_bindings" {
  description = <<EOT

    Example:

    user_bindings = [
      {
        user = "user1@email.com"
        cloud_roles = [
          "resource-manager.clouds.member",
        ],
        folder_roles = [
          {
            folder = "dev",
            roles  = ["viewer"]
          },
          {
            folder = "default",
            roles  = ["viewer"]
          },
        ],
      },
      {
        user = "user2@email.com"
        cloud_roles = [
          "resource-manager.clouds.member",
        ],
        folder_roles = [
          {
            folder = "prod",
            roles  = ["viewer"]
          },
          {
            folder = "dev",
            roles  = ["viewer", "storage.editor"]
          },
        ],
      },
    ]

  EOT

  type = list(object({
    user = string
    folder_roles = optional(list(object({
      folder = string
      roles  = list(string)
    })), [])
    cloud_roles = optional(list(string), [])
  }))
  default = []

  validation {
    condition = alltrue(
      [
        for entry in var.user_bindings :
        length(
          [
            for other in var.user_bindings :
            other.user == entry.user if other.user == entry.user
          ]
        ) == 1
      ]
    )
    error_message = "Duplicate «user» values are not allowed."
  }

  validation {
    condition = alltrue(
      [
        for entry in var.user_bindings :
        alltrue(
          [
            for folder_role in entry.folder_roles :
            length(
              [
                for other in entry.folder_roles :
                other.folder == folder_role.folder if other.folder == folder_role.folder
              ]
            ) == 1
          ]
        )
      ]
    )
    error_message = "Duplicate folder values within the same folder_roles array are not allowed."
  }
}
